from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'guides.views.index', name="index"),
    url(r'^join/', TemplateView.as_view(template_name='join.html'), name="join"),
    url(r'^thanks/', TemplateView.as_view(template_name='thanks.html'), name="thanks"),

    # Examples:
    # url(r'^$', 'pathwaze.views.home', name='home'),
    # url(r'^pathwaze/', include('pathwaze.foo.urls')),
    url(r'^accounts/', include('allauth.urls')),
    
    # URL for Contributors App
    #url(r'^contributor/', include('contributors.urls')), 

    # URL for Guides App
    url(r'^guides/', include('guides.urls'), name="guides"), 

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

# Uncomment the next line to serve media files in dev.
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
                            url(r'^__debug__/', include(debug_toolbar.urls)),
                            )
