from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    
    url(r'^profile/', 'contributors.views.profile', name="contributor_profile"),
    url(r'^edit_profile/', 'contributors.views.edit_profile', name="edit_contributor_profile"),

)

