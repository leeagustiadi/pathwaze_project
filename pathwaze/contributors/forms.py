from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import Contributor

class User_Profile_Form(ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

class Additional_Info_Form(ModelForm):
    class Meta:
        model = Contributor
        exclude = ('contributor',)

