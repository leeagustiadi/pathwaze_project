from django.shortcuts import render, render_to_response, RequestContext, Http404, HttpResponseRedirect

from .models import Contributor
from django.contrib.auth.models import User
from .forms import User_Profile_Form, Additional_Info_Form

def profile(request):
    user = request.user
    if user.is_active:
        contributor = Contributor.objects.get(contributor=user)
        context = {
             "user": user,
             "contributor":contributor,
        }
        return render_to_response("account/profile.html", context, context_instance=RequestContext(request))
    else:
        raise Http404

def edit_profile(request):
    if request.user.is_authenticated():
        instance_user = request.user
        instance_contributor = Contributor.objects.get(contributor=instance_user)
        user_profile_form = User_Profile_Form(request.POST or None, instance=instance_user)
        additional_info_form = Additional_Info_Form(request.POST or None, instance=instance_contributor)
        context = {
            "user_profile_form" : user_profile_form,
            "additional_info_form" : additional_info_form,
        }
        if user_profile_form.is_valid() and additional_info_form.is_valid():
            user_profile_edit = user_profile_form.save(commit=False)
            additional_info_edit = additional_info_form.save(commit=False)
            additional_info_edit.profile_pic = request.FILES['profile_pic']
            user_profile_edit.save()
            additional_info_edit.save()
            return HttpResponseRedirect ('http://pathwaze-workspace-agustiadi-3.c9.io/contributor/profile/')
        return render_to_response("account/edit_profile.html", context, context_instance=RequestContext(request))
    else:
        raise Http404
        