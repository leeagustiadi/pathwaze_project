from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

# Register your models here.
from .models import Contributor

# Define an inline admin descriptor for Contributor Model
# which acts a bit like a singleton
class ContributorInline(admin.StackedInline):
    model = Contributor
    can_delete = False
    verbose_name_plural = 'contributor'

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (ContributorInline, )
    list_display = ('username', 'email', 'first_name', 'last_name', 'contributor')
    

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
