from django.db import models
from django.contrib.auth.models import User

# Importing base.py file to access the DEFAULT_FILE_STORAGE for profile file uploading
from pathwaze.settings.base import DEFAULT_FILE_STORAGE

# Create your models here.
class Contributor(models.Model):
    contributor = models.OneToOneField(User)
    country = models.CharField(max_length=120, null=True, blank=True)
    city = models.CharField(max_length=120, null=True, blank=True)
    postal_code = models.CharField(max_length=20, null=True, blank=True)
    address = models.CharField(max_length=200, null=True, blank=True)
    profile_pic = models.ImageField(upload_to = DEFAULT_FILE_STORAGE + '/contributors/', null=True, blank=True)

    def __unicode__(self):
        return "Country: %s, City: %s, Postal_Code: %s"  %(self.country, self.city, self.postal_code)
        