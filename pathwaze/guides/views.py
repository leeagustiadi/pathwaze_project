import json

from itertools import chain

from django.shortcuts import render, render_to_response, RequestContext, Http404, redirect, HttpResponseRedirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.forms.models import model_to_dict
from django.core.context_processors import csrf

from .models import Guide, Step, Tag, Thank
from .forms import Request_Guide_Form, Thank_Form
from contributors.models import Contributor

def index(request):
    guides = Guide.objects.filter(active=True).order_by('-created')
    context = {
        "guides" : guides,
    }
    return render_to_response("index.html", context, context_instance=RequestContext(request))

def browse(request):
    guides = Guide.objects.filter(active=True)
    context = {
        "guides": guides,
    }
    return render_to_response("guides/browse_all.html", context, context_instance=RequestContext(request))

#def browse(request, slug):
#    if slug == None:
#        guides = Guide.objects.filter(active=True)
#        context = {
#            "guides": guides,
#        }
#        return render_to_response("guides/browse_all.html", context, context_instance=RequestContext(request))
#    else:
#        lowercase_slug = slug.lower()
#        guides = Guide.objects.filter(active=True, tag__tag=lowercase_slug)
#        album_tag = str(slug)
#        context = {
#            "guides": guides,
#            "album_tag": album_tag,
#        }
#        return render_to_response("guides/browse_with_tag.html", context, context_instance=RequestContext(request))

def search(request):
    q = request.GET.get('q','')
    if q == None or "":
        guides = Guide.objects.filter(active=True)
    else:
        guides = Guide.objects.filter(
            Q(slug__icontains=q)|
            Q(destination_name__icontains=q)|
            Q(destination_address__icontains=q)|
            Q(tag__tag__icontains=q),
            active=True
        )
        if not guides:
            query = q
            instance_user = request.user
            request_guide_form = Request_Guide_Form(request.POST or None)
            if request_guide_form.is_valid():
                request_guide_form.save()
                return HttpResponseRedirect (reverse('browse'))
            return render_to_response("search/no_result.html", locals(), context_instance=RequestContext(request))

    return render_to_response("guides/search.html", locals(), context_instance=RequestContext(request))

def search_within_browse(request):
    q = request.GET.get('q','')
    if q == None or "":
        guides = Guide.objects.filter(active=True)
    else:
        guides = Guide.objects.filter(
            Q(slug__icontains=q)|
            Q(destination_name__icontains=q)|
            Q(destination_address__icontains=q)|
            Q(tag__tag__icontains=q),
            active=True
        )
        if not guides:
            query = q
            instance_user = request.user
            request_guide_form = Request_Guide_Form(request.POST or None)
            if request_guide_form.is_valid():
                request_guide_form.save()
                return HttpResponseRedirect (reverse('browse'))
            return render_to_response("search/no_result.html", locals(), context_instance=RequestContext(request))
            
    return render_to_response("guides/browse_all.html", locals(), context_instance=RequestContext(request))

def search_within_NUS(request):
    q = request.GET.get('q','')
    if q == None or "":
        guides = Guide.objects.filter(active=True,tag__tag="NUS")
    else:
        guides = Guide.objects.filter(
            Q(slug__icontains=q)|
            Q(destination_name__icontains=q)|
            Q(destination_address__icontains=q),
            active=True,
            tag__tag="NUS"
        )
        if not guides:
            query = q
            instance_user = request.user
            request_guide_form = Request_Guide_Form(request.POST or None)
            if request_guide_form.is_valid():
                request_guide_form.save()
                return HttpResponseRedirect(reverse('browse'))
            return render_to_response("search/no_result.html", locals(), context_instance=RequestContext(request))
    return render_to_response("guides/search.html", locals(), context_instance=RequestContext(request))

def single(request, slug):
    if slug == "browse":
        guides = Guide.objects.filter(active=True)
        context = {
            "guides": guides,
        }
        return render_to_response("guides/browse_all.html", context, context_instance=RequestContext(request))
    else:
        guide = Guide.objects.get(slug=slug)
        guide.number_of_views += 1
        guide.save()
        steps = guide.step_set.all().order_by('step_number')
        contributor = Contributor.objects.get(contributor=guide.contributor)
        if request.user.is_authenticated():
            user = request.user
        else:
            user = None
        data = {'user': user, 'guide': guide }
        thank_form = Thank_Form(request.POST or None, initial=data)
        context = {
            "guide": guide,
            "steps": steps,
            "contributor": contributor,
            "thank_form": thank_form,
        }
        return render_to_response("guides/single.html", context, context_instance=RequestContext(request))
            
def thank(request, slug):
    if request.method == 'POST':
        guide = Guide.objects.get(slug=slug)
        response_data = {}
        if request.user.is_authenticated():
            thank = Thank(guide=guide, user=request.user)
        else:
            thank = Thank(guide=guide)
        thank.save()
        
        response_data['result'] = 'Thank successful!'
        response_data['guide'] = guide.destination_name
        response_data['user'] = request.user.username
        
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )