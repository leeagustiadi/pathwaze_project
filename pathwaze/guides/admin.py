from django.contrib import admin

# Import all the necessary models required to be registered on the Admin Panel
from .models import Guide, Step, Tag, Thank, Request

# Register your inline admin model here.
class StepInline(admin.TabularInline):
    model = Step
    extra = 1
    ordering = ('step_number',)
    
class TagInline(admin.TabularInline):
    model = Tag
    extra = 1
    prepopulated_fields = {"slug":('tag',)}

class ThankInline(admin.TabularInline):
    readonly_fields = ('user',)
    model = Thank
    extra = 1

# Register you normal admin model here.
class GuideAdmin(admin.ModelAdmin):
    list_display = ('destination_name', 'starting_point_name', 'contributor', 'created', 'updated', 'live_link')
    inlines = [StepInline, TagInline, ThankInline]
    search_fields = ['destination_name', 'destination_address', 'tag__tag']
    list_filter = ['created']
    prepopulated_fields = {"slug":('destination_name',)}
    readonly_fields = ['created', 'updated']
    
    class Meta:
        model = Guide
    
    def live_link(self, obj):
        link = "<a href= '/guides/%s/' target='_blank'>" %(obj.slug) + obj.destination_name + "</a>"
        return link
    live_link.allow_tags = True
        
admin.site.register(Guide, GuideAdmin)


class RequestAdmin(admin.ModelAdmin):
    class Meta:
        model = Request
        
admin.site.register(Request, RequestAdmin)

class ThankAdmin(admin.ModelAdmin):
    list_display = ('guide', 'user')
    
    class Meta:
        model = Thank
        
admin.site.register(Thank, ThankAdmin)