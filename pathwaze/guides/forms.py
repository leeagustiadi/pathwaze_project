from django.forms import ModelForm
from .models import Request, Thank

class Request_Guide_Form(ModelForm):
    class Meta:
        model = Request

class Thank_Form(ModelForm):
    class Meta:
        model = Thank
        fields = ('guide', 'user')