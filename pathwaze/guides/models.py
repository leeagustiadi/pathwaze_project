from django.db import models
from django.contrib.auth.models import User

#Importing from ImageKit
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

# Importing base.py file to access the DEFAULT_FILE_STORAGE for image uploading
from pathwaze.settings.base import DEFAULT_FILE_STORAGE

from contributors.models import Contributor


class Guide(models.Model):
    contributor = models.ForeignKey(User)
    destination_name = models.CharField(max_length=200)
    destination_address = models.CharField(max_length=300, null=True, blank=True)
    starting_point_name = models.CharField(max_length=200)
    starting_point_address = models.CharField(max_length=300, null=True, blank=True)
    cover_image = ProcessedImageField(upload_to=DEFAULT_FILE_STORAGE + '/cover_image/', format='JPEG', options={'quality': 30}, null=True, blank=True)
    slug = models.SlugField()
    destination_lat = models.FloatField(null=True, blank=True)
    destination_long = models.FloatField(null=True, blank=True)
    starting_point_lat = models.FloatField(null=True, blank=True)
    starting_point_long = models.FloatField(null=True, blank=True)
    google_map_URL = models.URLField(max_length=300, null=True, blank=True)
    active = models.BooleanField(default=True)
    number_of_views = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    
    def __unicode__(self):
        return str(self.destination_name)
        
    def get_profile_pic(self):
        contributor = Contributor.objects.get(contributor=self.contributor)
        profile_pic = contributor.profile_pic
        return profile_pic

class Step(models.Model):
    guide = models.ForeignKey(Guide)
    step_number = models.IntegerField(default=1)
    step_image = ProcessedImageField(upload_to=DEFAULT_FILE_STORAGE + '/step_image/', format='JPEG', options={'quality': 30} , null=True, blank=True)
    step_text = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    
    def __unicode__(self):
        return str(self.step_number)
        
class Tag(models.Model):
    guide = models.ForeignKey(Guide)
    tag = models.CharField(max_length=120)
    slug = models.SlugField()
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    
    def __unicode__(self):
        return self.tag
        
class Thank(models.Model):
    guide = models.ForeignKey(Guide)
    user = models.ForeignKey(User, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    
    def __unicode__(self):
        return str(self.created)
        
class Request(models.Model):
    destination_name = models.CharField(max_length=200)
    destination_address = models.CharField(max_length=300, null=True, blank=True)
    requester_email = models.EmailField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    
    def __unicode__(self):
        return self.destination_name
    
    
    
    

    