from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    
    #url(r'^create/', TemplateView.as_view(template_name='guides/create.html')),
    
    #url(r'^steps/', TemplateView.as_view(template_name='guides/steps.html')),
    
    url(r'^search/', 'guides.views.search', name="search_guides"),
    
    url(r'^browse/search/$', 'guides.views.search_within_browse', name="search_within_browse"),
    url(r'^browse/$', 'guides.views.browse', name="browse"),
    url(r'^browse/(?P<slug>.*)/$', 'guides.views.browse', name="browse_guides"),
    #url(r'^browse/NUS/$', 'guides.views.search_within_NUS', name="search_within_NUS"),
    
    url(r'^(?P<slug>.*)/thank_contributor/$', 'guides.views.thank', name="thank_contributor"),
    url(r'^(?P<slug>.*)/$', 'guides.views.single', name="single_guide"),
    
    
)

